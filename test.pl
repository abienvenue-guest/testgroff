#! /usr/bin/perl

#use 5.012;
use v5.20;
use warnings;
use utf8;

use IPC::Run3;
use Data::Dumper;

use constant EMPTY => q{};

use Getopt::Long;

my $patch = 0;

GetOptions( "patch!" => \$patch );

print
  "\n\n****************************************************** patch=$patch\n\n";

print "* man version:\n";
print `man -V`;

if ($patch) {

    delete local @ENV{ grep { $_ ne 'PATH' && $_ ne 'TMPDIR' } keys %ENV };

    local $ENV{LC_ALL} = 'C.UTF-8';

    local $ENV{MANROFFSEQ} = EMPTY;

    # set back to 80 when Bug#892423 is fixed in groff
    local $ENV{MANWIDTH} = 120;

    my $stdout;
    my $stderr;

    my @command;

    run3( ["env"], \undef, \$stdout, \$stderr );
    print "*\n** 'env' command output:\n*\n" . $stdout;

    print "*\n** perl " . '%' . "ENV HASH:\n*\n" . Dumper( \%ENV );

    print "*\n** 'locale' command output:\n";
    print `locale`;

    # ---------------------

    print "*\n* ldd on loc:\n*\n";
    print `ldd ./loc`;
    print "*\n* ldd on man:\n*\n";
    print `ldd /usr/bin/man`;

    # ----------------------

    @command = ("./loc");
    print "*\n\n***************************************\n** COMMAND:\n"
      . join( " ", @command ) . "\n";

    run3( \@command, \undef, \$stdout, \$stderr );

    print "*\n** STDOUT:\n*\n" . join( "\n", $stdout ) . "\n";
    print "*\n** STDERR:\n*\n" . join( "\n", $stderr ) . "\n";

    @command = qw(/usr/bin/man --warnings -E UTF-8 -l -Tutf8 -Z);
    push( @command, "auto-multiple-choice.fr.1" );
    print "*\n\n***************************************\n** COMMAND:\n"
      . join( " ", @command ) . "\n";

    run3( \@command, \undef, \$stdout, \$stderr );

    print "*\n** STDERR:\n*\n" . join( "\n", $stderr ) . "\n";

    # ----------------------

    @command = ( "perl", "-le", "" );
    print "*\n\n***************************************\n** COMMAND:\n"
      . join( " ", @command ) . "\n";

    run3( \@command, \undef, \$stdout, \$stderr );

    print "*\n** STDOUT:\n*\n" . join( "\n", $stdout ) . "\n";
    print "*\n** STDERR:\n*\n" . join( "\n", $stderr ) . "\n";

    # ----------------------

    @command = ("./loc");
    print "*\n\n***************************************\n** COMMAND:\n"
      . join( " ", @command ) . "\n";

    run3( \@command, \undef, \$stdout, \$stderr );

    print "*\n** STDOUT:\n*\n" . join( "\n", $stdout ) . "\n";
    print "*\n** STDERR:\n*\n" . join( "\n", $stderr ) . "\n";
} else {

    delete local $ENV{$_} for grep { $_ ne 'PATH' && $_ ne 'TMPDIR' } keys %ENV;

    local $ENV{LC_ALL} = 'C.UTF-8';

    local $ENV{MANROFFSEQ} = EMPTY;

    # set back to 80 when Bug#892423 is fixed in groff
    local $ENV{MANWIDTH} = 120;

    my $stdout;
    my $stderr;

    my @command = qw(man --warnings -E UTF-8 -l -Tutf8 -Z);
    push( @command, "auto-multiple-choice.fr.1" );

    run3( ["env"], \undef, \$stdout, \$stderr );
    print "*\n** 'env' command output:\n*\n" . $stdout;

    print "*\n** perl " . '%' . "ENV HASH:\n*\n" . Dumper( \%ENV );

    print "*\n** COMMAND:\n*\n" . join( " ", @command ) . "\n";

    run3( \@command, \undef, \$stdout, \$stderr );

    my $exitcode = $?;
    my $status   = ( $exitcode >> 8 );

    my @lines = split( /\n/, $stderr );

    print "*\n** STDERR:\n*\n" . join( "\n", @lines ) . "\n";
}
